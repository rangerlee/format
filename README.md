
format
============================================
支持GCC及MSVC的跨平台C++标准库字符串格式化工具库<br/>

---
###要解决的问题
实现C++标准字符串格式化，消除缓存越界风险，不依赖第三方库<br/>
MFC中CString也可以format的，但无法跨平台使用<br/>
boost库格式化较复杂，且boost库太过庞大，编译使用较麻烦


因库本身仅做封装，故支持的格式标准同C标准<br/>
在以下生产环境使用，长时间运行均稳定正常：<br/>
>
* MSVC 2005  (Windows 2003)
* GCC  4.1.2 (RedHat 5)

###format不能干什么
宽字符(wchar_t)格式字符串尚不支持<br/>

###format怎么使用
接口很简单，DEMO就省了<br/>

	#include <format>
	//通过返回值获取string
	std::string str = format("hello %s \n","OSC");

	//通过参数传入获得格式化结果
	format(str, "print number %d\n", rand());

	//当然va_list也必须得支持的，这就不列了

###其他
代码虽少，但很实用，<br/>
后续着力提升性能并增加MinGW编译器<br/>
>OSC主页：[http://my.oschina.net/rangerlee](http://my.oschina.net/rangerlee)<br/>
>联系方式：[rangerlee@foxmail.com](mailto:rangerlee@foxmail.com)